#!/bin/bash

set -e

echo "START script. Date: $(date -Is)"

cd "$(dirname "$0")"

source env/bin/activate

mkdir -p logs

python3 population_consolidated.py --config config.ini >> logs/cron.log 2>&1 

echo "FINISH script. Date: $(date -Is)"
