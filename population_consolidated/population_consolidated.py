import sys
import pandas as pd
import numpy as np
import pytz
import argparse
import configparser
import time
from collections import deque, defaultdict
from datetime import datetime, timezone, timedelta
from pymongo import InsertOne, DeleteMany, ReplaceOne, UpdateOne
from flowmaps.flowmaps import FlowMaps

tz = pytz.timezone('Europe/Madrid')


def fix_tz(date):
    return tz.localize(datetime.strptime(date.strftime('%Y-%m-%d'), '%Y-%m-%d'))

def get_data_for_date(date):
    start = tz.localize(datetime.strptime(date, '%Y-%m-%d'))
    end = start + timedelta(days=1)
    start, end = fix_tz(start), fix_tz(end)
    query = fm.query('mitma_mov.zone_movements').set_date_range(start, end).add_date_field().group_by(['date', 'id'], ['personas'])
    return query.get() #.to_list()

def get_population_matrix(dates, mitma_ids):
    M = np.zeros((len(dates), len(mitma_ids)))
    for n, date in enumerate(dates):
        if date[-2:] == '01': # print start of the month
            print('', date)
        cursor = get_data_for_date(date)
        i = dates.index(date)
        for doc in cursor:
            assert date == doc['date'], (date, doc)
            j = mitma_ids.index(doc['id'])
            M[i,j] = doc['personas']
    return M

def fill_missing_values_by_columns(M):
    for j in range(M.shape[1]):
        column = M[:, j].tolist()
        nonzero = [x for x in column if x > 0]
        default = sum(nonzero)/len(nonzero) if nonzero else 1 # mean of nonzero values
        for i in range(len(column)):
            if column[i] == 0:
                column[i] = default
        M[:, j] = column
    return M


def convert_population_to_layer(M, dates, layer):
    # get ids in target layer
    layer_ids = fm.layer(layer).get().ids()

    # get overlaps matrix
    O = fm.overlaps('mitma_mov', layer, collection=OVERLAPS_COLLECTION).get().to_matrix(row_ids=mitma_ids, col_ids=layer_ids)

    # project population using overlaps matrix
    M_proj = M*O

    # generate documents
    docs = get_documents_from_matrix(M_proj, layer, dates, layer_ids)

    return docs


def get_documents_from_matrix(M, layer, dates, layer_ids):
    docs = []
    for i, date in enumerate(dates):
        for j, iid in enumerate(layer_ids):
            doc = {
                'date': date,
                'id': iid,
                'layer': layer,
                'population': M[i, j],
                'type': 'population',
                'updated_at': datetime.now()
            }
            docs.append(doc)

    assert len(docs) == M.shape[0]*M.shape[1]
    return docs


if __name__ == '__main__':

    # ------------ read comanline -------------

    ap = argparse.ArgumentParser(description="Create and populate the MongoDB 'FlowBoard.cache_risk' collection.")

    ap.add_argument('-C','--config',dest="config_filename",required=True,help='Config file with the MongoDB credentials')
    ap.add_argument('--layer',dest="layer",default=None,required=False,help='layer_id of the layer to calculate the population')
    ap.add_argument('--dont-save',dest="dont_save",action="store_true",default=False,help='do not save the results to mongodb')

    args = ap.parse_args()

    config_path = args.config_filename

    print(f"STARTING at: {datetime.now()}")

    # ------------ read config file ----------------

    config = configparser.ConfigParser()
    config.read(config_path)
    cfg = config['DEFAULT']

    mongodb_params = {
        "host": [cfg['MONGO_HOST']],
        "username": cfg['MONGO_USERNAME'],
        "password": cfg['MONGO_PASSWORD'],
        "authSource": cfg['MONGO_AUTHSOURCE']
    }

    fm = FlowMaps(mongodb_params)
    
    DOCS_TYPE = cfg['DOCS_TYPE']
    WINDOW_SIZE = cfg.getint('WINDOW_SIZE')
    DEST_COLLECTION = cfg['DEST_COLLECTION']
    OVERLAPS_COLLECTION = cfg['OVERLAPS_COLLECTION']
    DONT_SAVE = args.dont_save
    if args.layer is not None:
        LAYERS = [args.layer]
    else:
        LAYERS = [l.strip() for l in cfg['LAYERS'].split(',')]

    print(f"running for layers: {LAYERS}")

    # get available dates
    last = fm.db['mitma_mov.zone_movements'].find_one(sort=[("evstart", -1)])['evstart']
    first = fm.db['mitma_mov.zone_movements'].find_one(sort=[("evstart", 1)])['evstart']
    first += timedelta(days=1) # first day of mobility seems to be incomplete...
    n_days = (last-first).days + 1
    first_str = first.strftime('%Y-%m-%d')
    last_str = last.strftime('%Y-%m-%d')
    dates = [(first + timedelta(days=x)).strftime('%Y-%m-%d') for x in range(n_days)]
    print(f"first date: {first_str}, last date: {last_str}, num days: {n_days}")
    assert len(dates) == n_days

    # get mitma ids
    mitma_ids = fm.layer('mitma_mov').project({'id': 1}).get().ids()
    print(f"found {len(mitma_ids)} mitma ids")

    # download the population matrix
    print(f"loading the population matrix")
    start_time = time.time()
    M = get_population_matrix(dates, mitma_ids)
    print("\n--- took %s seconds ---\n" % (time.time() - start_time))

    # fill missing values
    print(f"fixing missing values")
    M = fill_missing_values_by_columns(M)


    # ------------ delete previous PROVENANCE ----------------

    print('Deleting previous provenance')
    filters = {
        'storedIn': 'layers.data.consolidated',
        'keywords': {
            'type': 'population',
        }
    }
    fm.db['provenance'].delete_many(filters)

    # ------------ write temporal PROVENANCE ----------------

    filters = {'storedIn': 'mitma_mov.zone_movements'}
    source_provenance = list(fm.db['provenance'].find(filters))
    print(f'Writing provenance')
    doc = {
        'storedAt': tz.localize(datetime.now()),
        'lastStoredAt': None,
        'storedIn': 'layers.data.consolidated',
        'storedBy': sys.argv[0],
        'processedFrom': source_provenance,
        'keywords': {
            'type': 'population',
        },
        'status': 'started',
        'error': None,
    }
    result = fm.db['provenance'].insert_one(doc)
    provenance_id = result.inserted_id
    num_entries = 0


    # store mitma_mov layer documents
    docs = get_documents_from_matrix(M, 'mitma_mov', dates, mitma_ids)

    if DONT_SAVE:
        print('skipping... (do not save to mongodb)')

    else:
        print('storing population data for layer=mitma_mov')

        # ------------ delete previous documents ----------------
        print(f'Deleting previous documents in {DEST_COLLECTION} for type={DOCS_TYPE}, layer=mitma_mov')
        fm.db[DEST_COLLECTION].delete_many({'type': DOCS_TYPE, 'layer': 'mitma_mov'})

        # ------------ write documents to mongo ----------------

        operations = [InsertOne(doc) for doc in docs]
        num_entries += len(operations)

        print(f'writing {len(docs)} documents to: {DEST_COLLECTION}')

        fm.db[DEST_COLLECTION].bulk_write(operations, ordered=False)
        print("\n--- took %s seconds ---\n" % (time.time() - start_time))


    # convert population to each layer
    for layer in LAYERS:

        start_time = time.time()

        print(f"\nConverting population to layer={layer} using overlaps matrix")
        docs = convert_population_to_layer(M, dates, layer)

        if DONT_SAVE:
            print('skipping... (do not save to mongodb)')
            continue


        # ------------ delete previous documents ----------------

        print(f'Deleting previous documents in {DEST_COLLECTION} for type={DOCS_TYPE}, layer={layer}')
        fm.db[DEST_COLLECTION].delete_many({'type': DOCS_TYPE, 'layer': layer})


        # ------------ write documents to mongo ----------------

        try:
            operations = [InsertOne(doc) for doc in docs]
            num_entries += len(operations)

            print(f'writing {len(docs)} documents to: {DEST_COLLECTION}')

            fm.db[DEST_COLLECTION].bulk_write(operations, ordered=False)
            print("\n--- took %s seconds ---\n" % (time.time() - start_time))
        except Exception as e:
            # update provenance with error
            print(f'ERROR: {e}, updating provenance with error')
            update = {
                '$set': {
                    'status': 'failed',
                    'error': type(e).__name__,
                    'errorAt': tz.localize(datetime.now()),
                }
            }
            fm.db['provenance'].update_one({'_id': provenance_id}, update)
            exit()


    # ------------ finish-update PROVENANCE ----------------

    print(f'Updating provenance')
    update = {
        '$set': {
            'status': 'finished',
            'numEntries': num_entries,
            'lastStoredAt': tz.localize(datetime.now()),
        }
    }
    fm.db['provenance'].update_one({'_id': provenance_id}, update)

    print(f"FINISHED at: {datetime.now()}")

