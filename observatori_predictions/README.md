# Installation

Install python dependencies in a virtual environment:

	```bash
	# create and activate virtual environment
	virtualenv env --python=python3
	source env/bin/activate

	# The newer the better
	pip3 install --upgrade pip wheel

	# install script requirements using latest versions
	pip3 install -r requirements.txt
	```


# download_predictions.py

This script is meant to be launched in a CRON, every day after the new predictions are accesible through the endpoint (Xavier from Observatori said that predictions should be available at about 5:30)

The script downloads the predictions from the provided endpoint and stores the response in our mongodb, in the collection `layers.predictions` (configurable).


NOTE:

	The REST API (eve) is already configured to serve that collection, so the idea is to configure the Website created by Marc to get the predictions from the REST API.

	The problem is that adapting the Website is not trivial, as the whole arquitecture was built assuming the full predictions are loaded at the beginning from a file and afterwards it will just filter them based on the chosen date range, selected patch, etc. 


# download_predictions2.py

This script is meant to be launched in a CRON, every day after the new predictions are accesible through the endpoint (Xavier from Observatori said that predictions should be available at about 5:30)

It is an alternative to the previous script. Instead of saving the predictions to mongoDB, it generates a JSON file with the same format used by Marc's Website internally. This JSON has the advantage of having reduced size. The JSON is loaded by the Website, look at `src/store/reg_san_obs_09_predictions.js`.


The output JSON has the following format: 

	{
		"casos_reportats": {
			"patchId1": {
				"date1": [ ... ],  # Simulation results of the simulation starting on date1
				"date2": [ ... ],  # This lists contains 21 values, containing the predictions for
				"date3": [ ... ],  # the 21 days in advance starting on the given date
				...
			},
			"patchId2": {
				...
			},
			...
		},
		"casos_actius": {
			"patchId1": {
				"date1": [ ... ],
				"date2": [ ... ],
				"date3": [ ... ],
				...
			},
			"patchId2": {
				...
			},
			...
		}
	}

