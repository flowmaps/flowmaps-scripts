import json
import requests
import pytz
from pymongo import MongoClient, ReplaceOne
from datetime import datetime

from secrets import MONGODB_PARAMS


DATASET = 'observatori_reg_san_obs_09'
LAYER = 'reg_san_obs_09'
DEST_COLLECTION = 'layers.predictions'

tz = pytz.timezone('Europe/Madrid')


def init_mongodb(mongodb_params):
    database_name = 'FlowMaps'
    client = MongoClient(**mongodb_params)
    codec_options = client.codec_options.with_options(tzinfo=tz, tz_aware=True)
    db = client.get_database(database_name, codec_options=codec_options)
    return db


def main():

    print(f"starting at {datetime.now()}")

    with open('config.json') as f:
        cfg = json.load(f)

    db = init_mongodb(MONGODB_PARAMS)


    ########################
    # Download predictions
    ####
    url = cfg['predictions_url']

    today = datetime.now().strftime('%Y-%m-%d')

    url = url.format(date=today)
    print(f"downloading predictions from url: {url}")

    try:
        r = requests.get(url)
        documents = r.json()
    except Exception as e:
        print('Error downloading/parsing predictions:', e)
        exit(0)


    ########################
    # Store predictions in our db
    ####

    def build_id(doc):
        return "{dataset}:{data}:{data_simulacio}:{id}".format(**doc)

    # enrich documents
    for doc in documents:
        doc['dataset'] = DATASET  # unique identifier of the dataset
        doc['layer'] = LAYER
        doc['id'] = cfg['ids_mapping'].get(str(doc['id']))  # patch id
        doc['date_fetched'] = today
        doc['_id'] = build_id(doc)  # build a unique id to ensure no repetitions

    # insert/update documents
    operations = [ReplaceOne({'_id': doc['_id']}, doc, upsert=True) for doc in documents]
    collection = db[DEST_COLLECTION]
    print(f'writing {len(operations)} operations to the db')
    collection.bulk_write(operations, ordered=False)
    print(f"finishing at {datetime.now()}")


if __name__ == '__main__':
    main()

