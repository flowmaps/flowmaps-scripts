import sys
import json
import requests
import pytz
from datetime import datetime
from collections import defaultdict


def main():

    if len(sys.argv) < 2:
        print(f"usage: {sys.argv[0]} <output_file>")
        exit()

    output_filename = sys.argv[1]


    print(f"starting at {datetime.now()}")

    with open('config.json') as f:
        cfg = json.load(f)


    ########################
    # Download predictions
    ####
    url = cfg['predictions_url']

    today = datetime.now().strftime('%Y-%m-%d')

    url = url.format(date=today)
    print(f"downloading predictions from url: {url}")

    try:
        r = requests.get(url)
        documents = r.json()
    except Exception as e:
        print('Error downloading/parsing predictions:', e)


    ########################
    # Process documents
    ####

    # enrich documents
    for doc in documents:
        doc['id'] = cfg['ids_mapping'].get(str(doc['id']))  # patch id
        doc['date_fetched'] = today

    # ensure documents are sorted by data_simulacio
    documents = sorted(documents, key=lambda d: d['data_simulacio'])


    # # save predictions
    # with open('../src/static/predictions.json', 'w') as outfile:
    #     json.dump(documents, outfile)


    # build the indexed data that will be used by the web app
    # Extracted from the web app:
    # /*
    #     Each value has the following structure:
    #         variable = {
    #             patchId1: {
    #                 '2020-04-13': [...]
    #             },        
    #         }
    #         The array of values has the simulation results
    #         starting from the day of the object key.
    #     */ 
    columns = ['data_simulacio', 'casos_actius', 'casos_actius_error_inf', 'casos_actius_error_sup', 'casos_reportats', 'casos_reportats_error_inf', 'casos_reportats_error_sup', 'ocupacio_uci', 'ocupacio_uci_error_inf', 'ocupacio_uci_error_sup', 'ingresos_uci', 'ingresos_uci_error_inf', 'ingresos_uci_error_sup', 'hospitalitzacions', 'hospitalitzacions_error_inf', 'hospitalitzacions_error_sup', 'rt', 'rt_error_inf', 'rt_error_sup']
    data = defaultdict(lambda: defaultdict(lambda: defaultdict(list)))
    for column in columns:
        for doc in documents:
            data[column][doc['id']][doc['data']].append(doc[column])            

    # some checks
    # check that all final arrays have 21 elements
    for column, v in data.items():
        for patch, v1 in v.items():
            for date_sim, v2 in v1.items():
                assert len(v2) == 21
    # check that dates are sorted..
    for patch, v1 in data['data_simulacio'].items():
        for date_sim, v2 in v1.items():
            assert sorted(v2) == v2
    # delete unnecesary column
    del data['data_simulacio']

    with open(output_filename, 'w') as outfile:
        json.dump(data, outfile)



if __name__ == '__main__':
    main()

