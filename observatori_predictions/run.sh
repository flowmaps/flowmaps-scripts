#!/bin/bash

set -e

cd "$(dirname "$0")"

source env/bin/activate

python3 download_predictions.py
