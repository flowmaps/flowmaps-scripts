#!/bin/bash

# create and activate virtual environment
virtualenv env --python=python3
source env/bin/activate

# The newer the better
pip3 install --upgrade pip wheel

# install script requirements
pip3 install -r requirements.txt
# Uncomment this if you want latest versions
# (and remember running pip freeze > constraints.txt to record them!)
# pip3 install -r requirements.txt

