import epyestim.covid19 as covid19
import pandas as pd


DOCS_TYPE = 'covid19'
DEST_COLLECTION = 'layers.data.consolidated'


def compute_rt_per_id(dataframe):
    computed_dataframe = dataframe.reset_index()
    computed_dataframe['date'] = pd.to_datetime(computed_dataframe.date)
    cases_by_date = computed_dataframe.loc[:, ('date', 'CASOS_CONFIRMAT')]
    cases_by_date.columns = ['Date', 'Cases']
    cases_by_date = cases_by_date.set_index('Date', inplace=False)['Cases']
    ch_time_varying_r = covid19.r_covid(
        cases_by_date,
        smoothing_window=21,
        r_window_size=7,
        auto_cutoff=False
    )
    ch_time_varying_r = ch_time_varying_r.reset_index().rename(columns={'index': 'date'})
    ch_time_varying_r['date'] = ch_time_varying_r['date'].apply(lambda x: x.strftime('%Y-%m-%d'))
    new_dataframe = ch_time_varying_r.set_index('date')
    return new_dataframe

def compute_rt(data):
    # data es un dataframe con un doble indice: (id, date)
    result = data.groupby('id').apply(compute_rt_per_id)
    data['R_mean'] = result['R_mean']
    data['R_var'] = result['R_var']
    data['Q250'] = result['Q0.025']
    data['Q500'] = result['Q0.5']
    data['Q975'] = result['Q0.975']

def debug(data):
    import pdb; pdb.set_trace()

def aggregate_to_spain_level(data, cfg):
    #print('Aggregating provinces data to Spain level...')
    data = data.groupby('date').sum().reset_index()
    data['id'] = '0'
    data['ev'] = cfg['ev']
    data['layer'] = cfg['layer']
    data['type'] = DOCS_TYPE
    return data


SETTINGS = {
    ('ES.covid_cca', 'cnig_ccaa'): {
        'fields': {
            'num_casos': 'fill_with_zero',
            'num_casos_prueba_pcr': 'fill_with_zero',
            'num_casos_prueba_test_ac': 'fill_with_zero',
            'num_casos_prueba_ag': 'fill_with_zero',
            'num_casos_prueba_elisa': 'fill_with_zero',
            'num_casos_prueba_desconocida': 'fill_with_zero',
        },
        'add_fields': [
            {'name': 'population', 'how': 'population_from_mitma'},
            {'name': 'new_cases', 'source': 'num_casos', 'how': 'copy'},
            {'name': 'total_cases', 'source': 'new_cases', 'how': 'cumsum'},
            {'name': 'new_cases_mean_7', 'source': 'new_cases', 'how': 'mean', 'rolling_args': {'window': 7, 'min_periods': 1}},
            {'name': 'new_cases_mean_14', 'source': 'new_cases', 'how': 'mean', 'rolling_args': {'window': 14, 'min_periods': 1}},
            {'name': 'active_cases_7', 'source': 'new_cases', 'how': 'rolling_sum', 'rolling_args': {'window': 7, 'min_periods': 1}},
            {'name': 'active_cases_14', 'source': 'new_cases', 'how': 'rolling_sum', 'rolling_args': {'window': 14, 'min_periods': 1}},
            {'name': 'active_cases_7_by_100k', 'source': 'active_cases_7', 'how': 'by_100k'},
            {'name': 'active_cases_14_by_100k', 'source': 'active_cases_14', 'how': 'by_100k'},
            {'name': 'new_cases_by_100k', 'source': 'new_cases', 'how': 'by_100k'},
            {'name': 'total_cases_by_100k', 'source': 'total_cases', 'how': 'by_100k'}
        ],
    },
    ('ES.covid_cpro', 'cnig_provincias'): {
        'fields': {
            'num_casos': 'fill_with_zero',
            'num_casos_prueba_pcr': 'fill_with_zero',
            'num_casos_prueba_test_ac': 'fill_with_zero',
            'num_casos_prueba_ag': 'fill_with_zero',
            'num_casos_prueba_elisa': 'fill_with_zero',
            'num_casos_prueba_desconocida': 'fill_with_zero',
        },
        'add_fields': [
            {'name': 'population', 'how': 'population_from_mitma'},
            {'name': 'new_cases', 'source': 'num_casos', 'how': 'copy'},
            {'name': 'total_cases', 'source': 'new_cases', 'how': 'cumsum'},
            {'name': 'new_cases_mean_7', 'source': 'new_cases', 'how': 'mean', 'rolling_args': {'window': 7, 'min_periods': 1}},
            {'name': 'new_cases_mean_14', 'source': 'new_cases', 'how': 'mean', 'rolling_args': {'window': 14, 'min_periods': 1}},
            {'name': 'active_cases_7', 'source': 'new_cases', 'how': 'rolling_sum', 'rolling_args': {'window': 7, 'min_periods': 1}},
            {'name': 'active_cases_14', 'source': 'new_cases', 'how': 'rolling_sum', 'rolling_args': {'window': 14, 'min_periods': 1}},
            {'name': 'active_cases_7_by_100k', 'source': 'active_cases_7', 'how': 'by_100k'},
            {'name': 'active_cases_14_by_100k', 'source': 'active_cases_14', 'how': 'by_100k'},
            {'name': 'new_cases_by_100k', 'source': 'new_cases', 'how': 'by_100k'},
            {'name': 'total_cases_by_100k', 'source': 'total_cases', 'how': 'by_100k'},
        ],
        'derived_datasets': [
            {'ev': 'ES.covid_cpro.agg_ES', 'layer': 'spain', 'fn': aggregate_to_spain_level}
        ]
    },
    ('10.covid_cumun', 'cnig_municipios'): {
        'fields': {
            'total_cases': 'fill_with_previous',
            'cases_rate_x_100': 'fill_with_previous',
            'total_cases_14': 'fill_with_previous',
            'cases_14_x_100': 'fill_with_previous',
            'total_deceased': 'fill_with_previous',
            'deceased_rate_x_100': 'fill_with_previous',
        },
        'add_fields': [
            {'name': 'population', 'how': 'population_cnig_municipios'},
            {'name': 'new_cases', 'source': 'total_cases', 'how': 'diff_with_previous'},
            {'name': 'new_cases_mean_7', 'source': 'new_cases', 'how': 'mean', 'rolling_args': {'window': 7, 'min_periods': 1}},
            {'name': 'new_cases_mean_14', 'source': 'new_cases', 'how': 'mean', 'rolling_args': {'window': 14, 'min_periods': 1}},
            {'name': 'active_cases_7', 'source': 'new_cases', 'how': 'rolling_sum', 'rolling_args': {'window': 7, 'min_periods': 1}},
            {'name': 'active_cases_14', 'source': 'new_cases', 'how': 'rolling_sum', 'rolling_args': {'window': 14, 'min_periods': 1}},
            {'name': 'active_cases_7_by_100k', 'source': 'active_cases_7', 'how': 'by_100k'},
            {'name': 'active_cases_14_by_100k', 'source': 'active_cases_14', 'how': 'by_100k'},
            {'name': 'new_cases_by_100k', 'source': 'new_cases', 'how': 'by_100k'},
            {'name': 'total_cases_by_100k', 'source': 'total_cases', 'how': 'by_100k'},
        ],
    },
    ('09.covid_abs', 'abs_09'): {
        'fields': {
            'cases': 'fill_with_zero',
            'suspects': 'fill_with_zero',
            'negative': 'fill_with_zero',
        },
        'add_fields': [
            {'name': 'population', 'how': 'population_from_mitma'},
            {'name': 'new_cases', 'source': 'cases', 'how': 'copy'},
            {'name': 'total_cases', 'source': 'new_cases', 'how': 'cumsum'},
            {'name': 'new_cases_mean_7', 'source': 'new_cases', 'how': 'mean', 'rolling_args': {'window': 7, 'min_periods': 1}},
            {'name': 'new_cases_mean_14', 'source': 'new_cases', 'how': 'mean', 'rolling_args': {'window': 14, 'min_periods': 1}},
            {'name': 'active_cases_7', 'source': 'new_cases', 'how': 'rolling_sum', 'rolling_args': {'window': 7, 'min_periods': 1}},
            {'name': 'active_cases_14', 'source': 'new_cases', 'how': 'rolling_sum', 'rolling_args': {'window': 14, 'min_periods': 1}},
            {'name': 'active_cases_7_by_100k', 'source': 'active_cases_7', 'how': 'by_100k'},
            {'name': 'active_cases_14_by_100k', 'source': 'active_cases_14', 'how': 'by_100k'},
            {'name': 'new_cases_by_100k', 'source': 'new_cases', 'how': 'by_100k'},
            {'name': 'total_cases_by_100k', 'source': 'total_cases', 'how': 'by_100k'},
        ],
    },
    ('15.covid_abs', 'zbs_15'): {
        'fields': {
            'cases': 'fill_with_zero',
            'total_cases': 'fill_with_previous',
        },
        'add_fields': [
            {'name': 'population', 'how': 'population_from_mitma'},
            {'name': 'new_cases', 'source': 'cases', 'how': 'copy'},
            {'name': 'new_cases_mean_7', 'source': 'new_cases', 'how': 'mean', 'rolling_args': {'window': 7, 'min_periods': 1}},
            {'name': 'new_cases_mean_14', 'source': 'new_cases', 'how': 'mean', 'rolling_args': {'window': 14, 'min_periods': 1}},
            {'name': 'active_cases_7', 'source': 'new_cases', 'how': 'rolling_sum', 'rolling_args': {'window': 7, 'min_periods': 1}},
            {'name': 'active_cases_14', 'source': 'new_cases', 'how': 'rolling_sum', 'rolling_args': {'window': 14, 'min_periods': 1}},
            {'name': 'active_cases_7_by_100k', 'source': 'active_cases_7', 'how': 'by_100k'},
            {'name': 'active_cases_14_by_100k', 'source': 'active_cases_14', 'how': 'by_100k'},
            {'name': 'new_cases_by_100k', 'source': 'new_cases', 'how': 'by_100k'},
            {'name': 'total_cases_by_100k', 'source': 'total_cases', 'how': 'by_100k'},
        ],
    },
    ('07.covid_abs', 'zbs_07'): {
        'fields': {
            'cases': 'fill_with_zero',
        },
        'add_fields': [
            {'name': 'population', 'how': 'population_from_mitma'},
            {'name': 'new_cases', 'source': 'cases', 'how': 'copy'},
            {'name': 'total_cases', 'source': 'new_cases', 'how': 'cumsum'},
            {'name': 'new_cases_mean_7', 'source': 'new_cases', 'how': 'mean', 'rolling_args': {'window': 7, 'min_periods': 1}},
            {'name': 'new_cases_mean_14', 'source': 'new_cases', 'how': 'mean', 'rolling_args': {'window': 14, 'min_periods': 1}},
            {'name': 'active_cases_7', 'source': 'new_cases', 'how': 'rolling_sum', 'rolling_args': {'window': 7, 'min_periods': 1}},
            {'name': 'active_cases_14', 'source': 'new_cases', 'how': 'rolling_sum', 'rolling_args': {'window': 14, 'min_periods': 1}},
            {'name': 'active_cases_7_by_100k', 'source': 'active_cases_7', 'how': 'by_100k'},
            {'name': 'active_cases_14_by_100k', 'source': 'active_cases_14', 'how': 'by_100k'},
            {'name': 'new_cases_by_100k', 'source': 'new_cases', 'how': 'by_100k'},
            {'name': 'total_cases_by_100k', 'source': 'total_cases', 'how': 'by_100k'},
        ],
    },
    ('16.covid_abs', 'oe_16'): {
        'fields': {
            'cases': 'fill_with_zero',
            'total_cases': 'fill_with_previous',
            'pop': 'fill_with_previous'
        },
        'add_fields': [
            {'name': 'population', 'source': 'pop', 'how': 'copy'},
            {'name': 'new_cases', 'source': 'cases', 'how': 'copy'},
            {'name': 'new_cases_mean_7', 'source': 'new_cases', 'how': 'mean', 'rolling_args': {'window': 7, 'min_periods': 1}},
            {'name': 'new_cases_mean_14', 'source': 'new_cases', 'how': 'mean', 'rolling_args': {'window': 14, 'min_periods': 1}},
            {'name': 'active_cases_7', 'source': 'new_cases', 'how': 'rolling_sum', 'rolling_args': {'window': 7, 'min_periods': 1}},
            {'name': 'active_cases_14', 'source': 'new_cases', 'how': 'rolling_sum', 'rolling_args': {'window': 14, 'min_periods': 1}},
            {'name': 'active_cases_7_by_100k', 'source': 'active_cases_7', 'how': 'by_100k'},
            {'name': 'active_cases_14_by_100k', 'source': 'active_cases_14', 'how': 'by_100k'},
            {'name': 'new_cases_by_100k', 'source': 'new_cases', 'how': 'by_100k'},
            {'name': 'total_cases_by_100k', 'source': 'total_cases', 'how': 'by_100k'},
        ],
    },
    ('13.covid_abs', 'zon_bas_13'): {
        'fields': {
            'cases': 'fill_with_zero',
            'total_cases': 'fill_with_previous',
        },
        'add_fields': [
            {'name': 'population', 'how': 'population_from_mitma'},
            {'name': 'new_cases', 'source': 'cases', 'how': 'copy'},
            {'name': 'new_cases_mean_7', 'source': 'new_cases', 'how': 'mean', 'rolling_args': {'window': 7, 'min_periods': 1}},
            {'name': 'new_cases_mean_14', 'source': 'new_cases', 'how': 'mean', 'rolling_args': {'window': 14, 'min_periods': 1}},
            {'name': 'active_cases_7', 'source': 'new_cases', 'how': 'rolling_sum', 'rolling_args': {'window': 7, 'min_periods': 1}},
            {'name': 'active_cases_14', 'source': 'new_cases', 'how': 'rolling_sum', 'rolling_args': {'window': 14, 'min_periods': 1}},
            {'name': 'active_cases_7_by_100k', 'source': 'active_cases_7', 'how': 'by_100k'},
            {'name': 'active_cases_14_by_100k', 'source': 'active_cases_14', 'how': 'by_100k'},
            {'name': 'new_cases_by_100k', 'source': 'new_cases', 'how': 'by_100k'},
            {'name': 'total_cases_by_100k', 'source': 'total_cases', 'how': 'by_100k'},
        ],
    },
    ('06.covid_cumun', 'cnig_municipios'): {
        'fields': {
            'total_active': 'fill_with_previous',
            'total_cured': 'fill_with_previous',
            'total_cases': 'fill_with_previous',
            'total_deceases': 'fill_with_previous',
        },
        'add_fields': [
            {'name': 'population', 'how': 'population_cnig_municipios'},
            {'name': 'new_cases', 'source': 'total_cases', 'how': 'diff_with_previous'},
            {'name': 'new_cases_mean_7', 'source': 'new_cases', 'how': 'mean', 'rolling_args': {'window': 7, 'min_periods': 1}},
            {'name': 'new_cases_mean_14', 'source': 'new_cases', 'how': 'mean', 'rolling_args': {'window': 14, 'min_periods': 1}},
            {'name': 'active_cases_7', 'source': 'new_cases', 'how': 'rolling_sum', 'rolling_args': {'window': 7, 'min_periods': 1}},
            {'name': 'active_cases_14', 'source': 'new_cases', 'how': 'rolling_sum', 'rolling_args': {'window': 14, 'min_periods': 1}},
            {'name': 'active_cases_7_by_100k', 'source': 'active_cases_7', 'how': 'by_100k'},
            {'name': 'active_cases_14_by_100k', 'source': 'active_cases_14', 'how': 'by_100k'},
            {'name': 'new_cases_by_100k', 'source': 'new_cases', 'how': 'by_100k'},
            {'name': 'total_cases_by_100k', 'source': 'total_cases', 'how': 'by_100k'},
        ],
    },
    ('03.covid_cumun', 'cnig_municipios'): {
        'fields': {
            'cases': 'fill_with_zero',
            'casos_acum': 'fill_with_previous'
        },
        'add_fields': [
            {'name': 'population', 'how': 'population_cnig_municipios'},
            {'name': 'new_cases', 'source': 'cases', 'how': 'copy'},
            {'name': 'total_cases', 'source': 'casos_acum', 'how': 'copy'},
            {'name': 'new_cases_mean_7', 'source': 'new_cases', 'how': 'mean', 'rolling_args': {'window': 7, 'min_periods': 1}},
            {'name': 'new_cases_mean_14', 'source': 'new_cases', 'how': 'mean', 'rolling_args': {'window': 14, 'min_periods': 1}},
            {'name': 'active_cases_7', 'source': 'new_cases', 'how': 'rolling_sum', 'rolling_args': {'window': 7, 'min_periods': 1}},
            {'name': 'active_cases_14', 'source': 'new_cases', 'how': 'rolling_sum', 'rolling_args': {'window': 14, 'min_periods': 1}},
            {'name': 'active_cases_7_by_100k', 'source': 'active_cases_7', 'how': 'by_100k'},
            {'name': 'active_cases_14_by_100k', 'source': 'active_cases_14', 'how': 'by_100k'},
            {'name': 'new_cases_by_100k', 'source': 'new_cases', 'how': 'by_100k'},
            {'name': 'total_cases_by_100k', 'source': 'total_cases', 'how': 'by_100k'},
        ],
    },
    ('01.situacion_dis_san', 'cnig_municipios'): {
        'fields': {
            'casesPDIA': 'fill_with_zero',
            'cases': 'fill_with_zero',
            'new_hospitalised': 'fill_with_zero',
            'new_severe_hospitalised': 'fill_with_zero',
            'deceased': 'fill_with_zero',
        },
        'add_fields': [
            {'name': 'population', 'how': 'population_cnig_municipios'},
            {'name': 'new_cases', 'source': 'cases', 'how': 'copy'},
            {'name': 'total_cases', 'source': 'new_cases', 'how': 'cumsum'},
            {'name': 'new_cases_mean_7', 'source': 'new_cases', 'how': 'mean', 'rolling_args': {'window': 7, 'min_periods': 1}},
            {'name': 'new_cases_mean_14', 'source': 'new_cases', 'how': 'mean', 'rolling_args': {'window': 14, 'min_periods': 1}},
            {'name': 'active_cases_7', 'source': 'new_cases', 'how': 'rolling_sum', 'rolling_args': {'window': 7, 'min_periods': 1}},
            {'name': 'active_cases_14', 'source': 'new_cases', 'how': 'rolling_sum', 'rolling_args': {'window': 14, 'min_periods': 1}},
            {'name': 'active_cases_7_by_100k', 'source': 'active_cases_7', 'how': 'by_100k'},
            {'name': 'active_cases_14_by_100k', 'source': 'active_cases_14', 'how': 'by_100k'},
            {'name': 'new_cases_by_100k', 'source': 'new_cases', 'how': 'by_100k'},
            {'name': 'total_cases_by_100k', 'source': 'total_cases', 'how': 'by_100k'},
        ],
    },
    ('09.covid_region', 'reg_san_obs_09'): {
        'fields': {
            'CASOS_CONFIRMAT': 'fill_with_zero',
            'PCR': 'fill_with_zero',
            'TAR': 'fill_with_zero',
            'INGRESSOS_TOTAL': 'fill_with_zero',
            'INGRESSOS_CRITIC': 'fill_with_zero',
            'INGRESSATS_TOTAL': 'fill_with_zero',
            'INGRESSATS_CRITIC': 'fill_with_zero',
            'EXITUS': 'fill_with_zero',
            'CASOS_PCR': 'fill_with_zero',
            'CASOS_TAR': 'fill_with_zero',
            'POSITIVITAT_PCR_NUM': 'fill_with_zero',
            'POSITIVITAT_TAR_NUM': 'fill_with_zero',
            'POSITIVITAT_PCR_DEN': 'fill_with_zero',
            'POSITIVITAT_TAR_DEN': 'fill_with_zero',
            'VACUNATS_DOSI_1': 'fill_with_zero',
            'VACUNATS_DOSI_2': 'fill_with_zero',
        },
        'add_fields': [
            {'name': 'new_cases', 'source': 'CASOS_CONFIRMAT', 'how': 'copy'},
            {'name': 'total_cases', 'source': 'new_cases', 'how': 'cumsum'},
            {'name': 'new_cases_mean_7', 'source': 'new_cases', 'how': 'mean', 'rolling_args': {'window': 7, 'min_periods': 1}},
            {'name': 'new_cases_mean_14', 'source': 'new_cases', 'how': 'mean', 'rolling_args': {'window': 14, 'min_periods': 1}},
            {'name': 'active_cases_7', 'source': 'new_cases', 'how': 'rolling_sum', 'rolling_args': {'window': 7, 'min_periods': 1}},
            {'name': 'active_cases_14', 'source': 'new_cases', 'how': 'rolling_sum', 'rolling_args': {'window': 14, 'min_periods': 1}},
            {'how': 'custom', 'fn': compute_rt},
        ],
    }
}
