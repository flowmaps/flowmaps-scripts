import sys
import time
import json
import argparse
import logging
import pytz
import pymongo
import pandas as pd
from datetime import datetime, timedelta
from pymongo import InsertOne, DeleteMany, ReplaceOne, UpdateOne
from pymongo.errors import BulkWriteError

from settings import SETTINGS, DOCS_TYPE, DEST_COLLECTION

tz = pytz.timezone('Europe/Madrid')


class NegativeCases(Exception):
    def __init__(self, ev, field):
        self.ev = ev
        self.field = field
        self.message = f"Negative value detected for cases in field {field} at ev {ev}."
        super().__init__(self.message)


def init_mongodb(mongodb_params):
    database_name = 'FlowMaps'
    client = pymongo.MongoClient(**mongodb_params)
    codec_options = client.codec_options.with_options(tzinfo=tz, tz_aware=True)
    db = client.get_database(database_name, codec_options=codec_options)
    return db


def get_ev_layer_ids(db, ev, layer):
    filters = {'ev': ev, 'layer': layer}
    doc = db['layers.data.ids'].find_one(filters)
    if doc is None:
        raise Exception(f'Missing entry in layers.data.ids for: {filters}')
    return doc['ids']


def get_dataset(db, ev, layer):
    pipeline = [
        {
            '$match': {
                'ev': ev,
                'layer': layer,
            }
        },
        {
            '$addFields': {
                'date': {
                    '$dateToString': {
                        'format': '%Y-%m-%d', 
                        'date': '$evstart',
                        'timezone': 'Europe/Madrid',
                    }
                },
            }
        },
    ]
    cursor = db['layers.data'].aggregate(pipeline)
    df = pd.DataFrame(cursor)

    # Ensure only one entry per id and day 
    # WARN: this assumes only one entry per id and day
    df = df.groupby(['id', 'date']).first().reset_index()

    return df


def get_mitma_population(db, ev, layer, average=True, first_date='2020-02-15', last_date='2020-02-29'):
    """
    Returns approximate population for all the areas associated with one EV.

    It first looks in layers.data.ids for the ids of the areas associated with the EV.
    Example: 
        layer='cnig_municipios', ev='10.covid_cumun'
        for the dataset containing the covid data for municipalities in Valencia,
        we just want to download the population for Valencia municipalities, not all the layer

    Then, just for those ids in the layer, downloads the precalculated population for 
    the selected date range from layers.data.consolidated. This population
    comes from aggregating maestra2 from MITMA mobility dataset, and it is
    projected to the layer using the population overlaps

    Optional: return the average population in that date range instead of the daily population
    """
    pipeline = [
        {
            '$match': {
                'ev': ev, 
                'layer': layer
            }
        }, {
            '$unwind': {
                'path': '$ids'
            }
        }, {
            '$lookup': {
                'from': 'layers.data.consolidated', 
                'let': {
                    'id': '$ids'
                }, 
                'pipeline': [
                    {
                        '$match': {
                            'type': 'population', 
                            'layer': layer,
                            'date': {'$gte': first_date, '$lte': last_date},
                        }
                    }, {
                        '$match': {
                            '$expr': {
                                '$eq': [
                                    '$$id', '$id'
                                ]
                            }
                        }
                    }
                ], 
                'as': 'population'
            }
        }, {
            '$unwind': {
                'path': '$population'
            }
        }, {
            '$project': {
                'population': '$population.population',
                'date': '$population.date',
                'id': '$ids',
            }
        }
    ]
    cursor = db['layers.data.ids'].aggregate(pipeline)
    df = pd.DataFrame(cursor)

    if df.empty:
        raise Exception(f'population missing for layer {layer}')

    # Ensure only one entry per id and day 
    df = df.groupby(['id', 'date']).first().reset_index()

    if average:
        df = df.groupby(['id'])['population'].mean()

    return df


def get_population_cnig_municipios(db, ev, layer):
    pipeline = [
        {
            '$addFields': {
                'id': {
                    '$substr': [
                        '$cod_ine', 0, 5
                    ]
                }
            }
        }, {
            '$project': {
                'id': 1, 
                'population': '$poblacion_muni'
            }
        }
    ]
    cursor = db['cnig.municipios'].aggregate(pipeline)

    df = pd.DataFrame(cursor)

    df.drop_duplicates(subset='id', inplace=True)

    return df.set_index('id')['population']


def consolidate_dataframe(db, ev, layer, df):

    # get the ids for which there should be data
    ids = get_ev_layer_ids(db, ev, layer)

    # calculate the date range for which there should be data for every day
    min_date_str = df['date'].min()
    max_date_str = df['date'].max()
    min_date = datetime.strptime(min_date_str, '%Y-%m-%d')
    max_date = datetime.strptime(max_date_str, '%Y-%m-%d')
    num_days = (max_date - min_date).days
    dates = [min_date+timedelta(days=n) for n in range(num_days+1)]
    dates = [d.strftime('%Y-%m-%d') for d in dates]
    assert dates[0] == min_date_str and dates[-1] == max_date_str, f"min_date: {min_date_str}, max_date: {max_date_str}, dates: {dates}"
    logging.info(f"- available dates: {dates[0]} - {dates[-1]} ({num_days} days)")

    # create dataframe from the fields contained in 'd'
    data = pd.DataFrame(df['d'].to_list())
    data['id'] = df['id'].to_list()
    data['date'] = df['date'].to_list()
    data = data.set_index(['id', 'date'])

    # ensure one value for each date and id
    # reindex with full index, and fill missing values with NaN
    data = data.reindex(pd.MultiIndex.from_product([ids, dates], names=['id', 'date']))

    # ensure values are sorted by date (important for the cumsum and ffill operations)
    data = data.sort_index()

    # fill missing values in each column depending on its type
    for column, column_type in SETTINGS[(ev, layer)]['fields'].items():

        # for daily data (e.g. new_cases) fill missing values with 0
        if column_type == 'fill_with_zero':
            data[column].fillna(0, inplace=True)

        # for cummulative data, if something is missing fill with previous
        if column_type == 'fill_with_previous':
            data[column] = data[column].groupby('id').ffill()

    # add additional columns
    for field in SETTINGS[(ev, layer)]['add_fields']:

        if field['how'] == 'copy':
            data[field['name']] = data[field['source']]

        if field['how'] == 'cumsum':
            data[field['name']] = data[field['source']].groupby('id').cumsum()

        if field['how'] == 'diff_with_previous':
            data[field['name']] = data[field['source']].groupby('id').diff(1).fillna(0)

        if field['how'] == 'by_100k':
            data[field['name']] = (data[field['source']]*100000)/data['population']

        if field['how'] == 'mean':
            rolling_args = field.get('rolling_args', {})
            data[field['name']] = data.reset_index().set_index('date').groupby('id')[field['source']].rolling(**rolling_args).mean()

        if field['how'] == 'rolling_sum':
            rolling_args = field.get('rolling_args', {})
            data[field['name']] = data.reset_index().set_index('date').groupby('id')[field['source']].rolling(**rolling_args).sum()

        if field['how'] == 'population_from_mitma':
            population = get_mitma_population(db, ev, layer, **field.get('args', {}))
            data[field['name']] = data.join(population, on='id')['population']

        if field['how'] == 'population_cnig_municipios':
            population = get_population_cnig_municipios(db, ev, layer, **field.get('args', {}))
            data[field['name']] = data.join(population, on='id')['population']

        if field['how'] == 'custom':
            # execute custom function and get a column as return
            column = field['fn'](data)

            # save the returned column or value only if a name is provided for the column
            # otherwise the function itself is expected to modify the dataframe
            if 'name' in field and column is not None:
                data[field['name']] = column

    # fill fields common to all documents
    data['type'] = DOCS_TYPE
    data['layer'] = layer
    data['ev'] = ev

    if ( data['new_cases'] < 0 ).sum() > 0:
        logging.info(f"- Warning, negative cases detected in ev {ev}")
        # raise NegativeCases(ev, 'new_cases')

    if ( data['total_cases'] < 0 ).sum() > 0:
        logging.info(f"- Warning, negative cases detected in ev {ev}")
        # raise NegativeCases(ev, 'total_cases')

    # fill missing flag
    missing = data.index.difference(df.set_index(['id', 'date']).index)
    data['was_missing'] = False
    data.loc[missing, 'was_missing'] = True

    return data


def write_dataframe(db, ev, layer, df, source_ev=None, source_layer=None):
    """
    Write new dataframe to layers.data.consolidated, deleting old documents and 
    updating the provenance.

    The steps performed are the following:
    1. delete previous documents:
        1.1 mark previous provenance as "deleting"
        1.2 delete previous documents
        1.3 delete previous provenance
    2. write new documents:
        2.1 write temporal provenance as "writing"
        2.2 write documents
        2.3 update provenance with error or success 
    """

    # mark old provenance as deleting
    logging.info(f'- Marking previous provenance as deleting')
    filters = {
        'storedIn': DEST_COLLECTION,
        'keywords': {
            'type': DOCS_TYPE,
            'ev': ev,
            'layer': layer,
        }
    }
    old_provenance = db['provenance'].find_one(filters)
    if old_provenance:
        update = {
            '$set': {
                'status': 'deleting',
                'startedDeletingAt': tz.localize(datetime.now()),
            }
        }
        db['provenance'].update_one({'_id': old_provenance['_id']}, update)
    else:
        logging.info(f'- WARN: missing previous provenance {filters}')

    # delete previous documents
    start_time = time.time()
    filters = {'ev': ev, 'layer': layer, 'type': DOCS_TYPE}
    logging.info(f'  - Deleting previous documents matching: {filters}')
    db[DEST_COLLECTION].delete_many(filters)
    logging.info("    (took %s seconds)" % (time.time() - start_time))
    
    # delete previous provenance
    logging.info('- Deleting previous provenance')
    filters = {
        'storedIn': DEST_COLLECTION,
        'keywords': {
            'type': DOCS_TYPE,
            'ev': ev,
            'layer': layer,
        }
    }
    db['provenance'].delete_many(filters)

    # write new temporal provenance
    source_ev = source_ev or ev
    source_layer = source_layer or layer
    filters = {'storedIn': 'layers.data', 'keywords.ev': source_ev, 'keywords.layer': source_layer}
    source_provenance = db['provenance'].find_one(filters)
    logging.info(f'- Writing new provenance')
    doc = {
        'storedAt': tz.localize(datetime.now()),
        'lastStoredAt': None,
        'storedIn': DEST_COLLECTION,
        'storedBy': sys.argv[0],
        'processedFrom': [source_provenance],
        'keywords': {
            'type': DOCS_TYPE,
            'ev': ev,
            'layer': layer,
        },
        'status': 'started',
        'error': None,
    }
    result = db['provenance'].insert_one(doc)
    provenance_id = result.inserted_id

    # write new documents
    try:
        start_time = time.time()
        docs = df.to_dict('records')
        operations = [InsertOne(doc) for doc in docs]
        logging.info(f'  - Writing {len(docs)} documents')
        db[DEST_COLLECTION].bulk_write(operations, ordered=False)
        logging.info("    (took %s seconds)" % (time.time() - start_time))
    except Exception as e:
        # update provenance with error
        logging.info(f'- ERROR: {e}, updating provenance with error')
        update = {
            '$set': {
                'status': 'failed',
                'error': type(e).__name__,
                'errorAt': tz.localize(datetime.now()),
            }
        }
        db['provenance'].update_one({'_id': provenance_id}, update)
    else:
        # update temporal provenance with no errors
        logging.info(f'- Updating provenance with no errors')
        update = {
            '$set': {
                'status': 'finished',
                'numEntries': len(operations),
                'lastStoredAt': tz.localize(datetime.now()),
            }
        }
        db['provenance'].update_one({'_id': provenance_id}, update)


def consolidate_dataset(db, ev, layer):

    if (ev, layer) not in SETTINGS:
        raise Exception(f'EV and layer ({ev}, {layer}) not found in settings.py. \nAvailable pairs: {list(SETTINGS.keys())}')

    # get the raw dataset
    df = get_dataset(db, ev, layer)

    # consolidate dataframe

    try:
        data = consolidate_dataframe(db, ev, layer, df)

        # write data updating provenance
        write_dataframe(db, ev, layer, data.reset_index())

        # execute additional transformations to obtain derived datasets
        for cfg in SETTINGS[(ev, layer)].get('derived_datasets', []):

            # execute transformation
            derived_data = cfg['fn'](data, cfg)

            # write to db
            write_dataframe(db, cfg['ev'], cfg['layer'], derived_data.reset_index(), source_ev=ev, source_layer=layer)
    except NegativeCases as e:
        print(f"WARNING. Negative cases detected for ev {e.ev} in field {e.field} ")
        print("Skipping dataset consolidation")

if __name__ == '__main__':

    ap = argparse.ArgumentParser(description="")

    ap.add_argument('--config',dest="config",required=True,type=str,help='')
    ap.add_argument('--ev',dest="ev",required=False,type=str,help='')
    ap.add_argument('--layer',dest="layer",required=False,type=str,help='')
    ap.add_argument('--loglevel',dest="loglevel",default="INFO",required=False,type=str,help='')

    args = ap.parse_args()

    logging.basicConfig(level=getattr(logging, args.loglevel.upper()))

    logging.info(f"STARTING at: {datetime.now()}")

    with open(args.config) as f:
        config = json.load(f)

    db = init_mongodb(config['mongo_params'])

    if args.ev and args.layer:
        consolidate_dataset(db, args.ev, args.layer)
    else:
        for ev, layer in SETTINGS:
            logging.info(f'Starting ev: {ev}, layer: {layer}')
            consolidate_dataset(db, ev, layer)

    logging.info(f"FINISHING at: {datetime.now()}")

