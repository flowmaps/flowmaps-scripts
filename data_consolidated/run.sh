#!/bin/bash

set -e

echo "START script. Date: $(date -Is)"

cd "$(dirname "$0")"

source env/bin/activate

mkdir -p logs

python3 data_consolidated.py --config config.json 2>&1 | tee logs/cron.log

echo "FINISH script. Date: $(date -Is)"

