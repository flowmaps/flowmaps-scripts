import geopandas as gpd
from scipy.sparse import csr_matrix
import numpy as np


def add_id_to_features_properties(geojson):
    for feat in geojson['features']:
        feat['properties']['id'] = feat['id']
    return geojson

    
def build_overlaps_matrix(df, row_loc, col_loc, value_loc, row_ids, col_ids):
    data = []
    col = []
    row = []
    for i, doc in df.iterrows():
        i = row_ids.index(doc[row_loc])
        j = col_ids.index(doc[col_loc])
        data.append(doc[value_loc])
        row.append(i)
        col.append(j)
    O = csr_matrix((data, (row, col)), shape=(len(row_ids), len(col_ids)))
    return O.toarray()



def compute_population_overlaps(grid, df1, df2, id1, id2, normalize_sum_one=True):
    # Compute the overlaps for projection layer1->grid
    print('  - computing intersection (layer1 vs grid)')
    overlaps = gpd.overlay(df1, grid, how='intersection')
    overlaps['area'] = overlaps.geometry.to_crs("epsg:3857").area # Note: area must be calculated using a projected CRS: https://geopandas.readthedocs.io/en/latest/docs/reference/api/geopandas.GeoSeries.area.html

    # Compute the grid ratio: grid_ratio contains the ratio of each cell contained by the intersection (if the cell is fully contained it is 1)
    totals = overlaps.groupby('GRD_ID')['area'].sum()
    overlaps['grid_total'] = overlaps['GRD_ID'].apply(lambda x: totals.loc[x])
    overlaps['grid_ratio'] = overlaps['area']/overlaps['grid_total']
    
    # Compute the population on each intersection, from the total population on each cell
    overlaps['pop'] = overlaps['TOT_P'] * overlaps['grid_ratio']
    
    # Compute population ratios for projection layer1->grid, ensuring they sum one for each area in layer1 
    pop_totals = overlaps.groupby(id1)['pop'].sum()
    overlaps['area1_total_pop'] = overlaps[id1].apply(lambda x: pop_totals.loc[x])
    overlaps['ratio_1'] = overlaps['pop'] / overlaps['area1_total_pop']
    
    # Compute the overlaps for projection overlaps->layer2
    print('  - computing intersection (partial overlaps vs layer2)')
    overlaps['inters_total_area'] = overlaps.geometry.to_crs("epsg:3857").area
    overlaps2 = gpd.overlay(overlaps, df2, how='intersection')
    overlaps2['area'] = overlaps2.geometry.to_crs("epsg:3857").area
    overlaps2['ratio_2'] = overlaps2['area']/overlaps2['inters_total_area']
        
    # Combine ratios
    # The first ratio is the population ratio for projection area1->grid
    # The second ratio is the geographical ratio for the projection grid->area2
    overlaps2['ratio'] = overlaps2['ratio_1'] * overlaps2['ratio_2']
    overlaps2 = overlaps2.groupby([id1, id2])['ratio'].sum().reset_index()
    
    # Normalize ratio to sum 1
    print('  - normalize ratios')
    totals = overlaps2.groupby(id1)['ratio'].sum()
    overlaps2['total_ratio'] = overlaps2[id1].apply(lambda x: totals.loc[x])
    overlaps2['ratio_norm'] = overlaps2['ratio'] / overlaps2['total_ratio']

    return overlaps2

