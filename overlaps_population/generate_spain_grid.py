import geopandas as gpd
import pandas as pd
import argparse

# ------------ read comandline args -------------

ap = argparse.ArgumentParser(description="")
ap.add_argument('--population-filename',dest='population_filename',required=True,type=str,help='Population CSV filename. Example: data/GEOSTAT-grid-POP-1K-2011-V2-0-1/Version 2_0_1/GEOSTAT_grid_POP_1K_2011_V2_0_1.csv')
ap.add_argument('--grid-filename',dest='grid_filename',required=True,type=str,help='Grid shapefile. Example: data/GEOSTAT-grid-POP-1K-2011-V2-0-1/Version 2_0_1/GEOSTATReferenceGrid/Grid_ETRS89_LAEA_1K-ref_GEOSTAT_POP_2011_V2_0_1.shp')
ap.add_argument('--filtered-grid-filename',dest='spanish_grid_filename',default='data/grid_spain.shp',type=str,help="Output grid shapefile containing only Spanish grid")

args = ap.parse_args()

config_path = args.config_filename

# read grid shapefile
# filename = 'data/GEOSTAT-grid-POP-1K-2011-V2-0-1/Version 2_0_1/GEOSTATReferenceGrid/Grid_ETRS89_LAEA_1K-ref_GEOSTAT_POP_2011_V2_0_1.shp'
filename = args.grid_filename
grid = gpd.read_file(filename)

# read population CSV data
# filename = 'data/GEOSTAT-grid-POP-1K-2011-V2-0-1/Version 2_0_1/GEOSTAT_grid_POP_1K_2011_V2_0_1.csv'
filename = args.population_filename
data = pd.read_csv(filename)

# merge grid and data
df = grid.merge(data, on='GRD_ID')

# filter spain data
spain = df[(df['YEAR']==2011) & (df['DATA_SRC']=='ES')]

assert spain.shape[0] == spain['GRD_ID'].nunique()

# save
spain.to_file(args.spanish_grid_filename)

