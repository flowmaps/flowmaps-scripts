import argparse
import json
import time
import pytz
import pandas as pd
import geopandas as gpd
import numpy as np
from datetime import datetime, timedelta
from pymongo import InsertOne, DeleteMany, ReplaceOne, UpdateOne

from flowmaps.flowmaps import FlowMaps
import utils

# ------------ read comandline args -------------

ap = argparse.ArgumentParser(description="")

ap.add_argument('-C','--config',dest="config_filename",required=True,help='Config file with the MongoDB credentials')
ap.add_argument('--layer1',required=True,type=str,help='layer id')
ap.add_argument('--layer2',required=True,type=str,help='layer id')
ap.add_argument('--population-filename',dest='population_filename',type=str,default='data/GEOSTAT_grid_POP_1K_2011_V2_0_1.csv',help='Population CSV filename. Example: data/GEOSTAT-grid-POP-1K-2011-V2-0-1/Version 2_0_1/GEOSTAT_grid_POP_1K_2011_V2_0_1.csv')
ap.add_argument('--filtered-grid-filename',dest='spanish_grid_filename',type=str,default='data/grid_spain.shp',help='Spanish grid shapefile. Example: data/grid_spain.shp')

args = ap.parse_args()

config_path = args.config_filename

# ------------ read config file ----------------

with open(config_path) as f:
    cfg = json.load(f)

DEST_COLLECTION = 'layers.overlaps_population'
LAYERL, LAYERM = sorted([args.layer1, args.layer2]) # sorted lexicographically

print(f'Layer l: {LAYERL}, layer m: {LAYERM} (sorted lexicographically)')

# ------------ init mongodb ----------------

tz = pytz.timezone('Europe/Madrid')
fm = FlowMaps(cfg['mongo_params'])
db = fm.db


# ------------ download layers ----------------


print('downloading geojson layers')
layer_l_ids = fm.layer(LAYERL).ids()
layer_m_ids = fm.layer(LAYERM).ids()

layer_l_geojson = fm.layer(LAYERL).get().to_geojson()
layer_m_geojson = fm.layer(LAYERM).get().to_geojson()

layer_l_geojson = utils.add_id_to_features_properties(layer_l_geojson)
layer_m_geojson = utils.add_id_to_features_properties(layer_m_geojson)

layer_l = gpd.GeoDataFrame.from_features(layer_l_geojson['features'])
layer_m = gpd.GeoDataFrame.from_features(layer_m_geojson['features'])

layer_l = layer_l.set_crs("EPSG:4326") # by default, geojsons are assumed to be in CRS="EPSG:4326"
layer_m = layer_m.set_crs("EPSG:4326")

layer_l = layer_l.rename(columns={'id':'layer_l_id'})
layer_m = layer_m.rename(columns={'id':'layer_m_id'})


print('loading spanish grid')
grid = gpd.read_file(args.spanish_grid_filename, crs="epsg:4258") # the GEOSTAT sapefile is in CRS="epsg:4258", source: https://ec.europa.eu/eurostat/web/gisco/geodata/reference-data/population-distribution-demography/geostat
dtypes={'TOT_P': int, "GRD_ID": str,"CNTR_CODE": str,"METHD_CL": str,"YEAR": str,"DATA_SRC": str,"TOT_P_CON_DT": str}
population = pd.read_csv(args.population_filename, dtype=dtypes)
population = population[population['DATA_SRC']=='ES'][['TOT_P', 'GRD_ID']]
grid = pd.merge(grid, population, left_on='GRD_ID', right_on='GRD_ID')
grid.set_crs("EPSG:4326", inplace=True) # convert to the same CRS as the geojsons

# ------------ compute overlaps ----------------

print('computing overlaps')
overlaps_l = utils.compute_population_overlaps(grid, layer_l, layer_m, 'layer_l_id', 'layer_m_id')
overlaps_m = utils.compute_population_overlaps(grid, layer_m, layer_l, 'layer_m_id', 'layer_l_id')

overlaps_l = overlaps_l.set_index(['layer_l_id', 'layer_m_id'])['ratio'].to_dict()
overlaps_m = overlaps_m.set_index(['layer_m_id', 'layer_l_id'])['ratio'].to_dict()


# ------------ generate documents ----------------

print('generating documents')

docs = []

for i, lid in enumerate(layer_l_ids):
	for j, mid in enumerate(layer_m_ids):
		if (lid, mid) in overlaps_l or (mid, lid) in overlaps_m:
			doc = {
				'l': {
					'id': lid,
					'layer': LAYERL,
					'ratio': overlaps_l.get((lid, mid), 0)
				},
				'm': {
					'id': mid,
					'layer': LAYERM,
					'ratio': overlaps_m.get((mid, lid), 0)
				}
			}
			docs.append(doc)


# ------------ write documents to mongo ----------------

print('Deleting previous documents')
filters = {
    'l.layer': LAYERL,
    'm.layer': LAYERM,
}
db[DEST_COLLECTION].delete_many(filters)

operations = [InsertOne(doc) for doc in docs]
print(f'Writing {len(docs)} documents to: {DEST_COLLECTION}', end='...')

start_time = time.time()
db[DEST_COLLECTION].bulk_write(operations, ordered=False)
print(" took %s seconds ---\n" % (time.time() - start_time))
