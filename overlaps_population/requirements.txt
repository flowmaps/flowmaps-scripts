geopandas
numpy
pymongo
pytz
pandas
scipy
git+https://gitlab.bsc.es/flowmaps/pyflowmaps.git#egg=flowmaps