
# Generate Population Overlaps between two geographical layers

The scripts in this directory can be used to generate the population overlap between two layers, i.e. the number of people that share each pair of polygons from two different layers. This can be used to translate population-related data (e.g. COVID-19 incidence, mobility, etc) from one layer to another.

The population density grid for Europe can be downloaded from here (select 2011 for more accurate data):

https://ec.europa.eu/eurostat/web/gisco/geodata/reference-data/population-distribution-demography/geostat

After downloading and unziping the GEOSTAT files, run the script `generate_spain_grid.py` to filter and store the grid just for Spain.


Example:

    ```bash
    mkdir -p data
    # Download the data from https://ec.europa.eu/eurostat/web/gisco/geodata/reference-data/population-distribution-demography/geostat
    # Once data is downloaded, next script pre-processes it
    python generate_spain_grid.py --population-filename data/GEOSTAT-grid-POP-1K-2011-V2-0-1/Version\ 2_0_1/GEOSTAT_grid_POP_1K_2011_V2_0_1.csv --grid-filename data/GEOSTAT-grid-POP-1K-2011-V2-0-1/Version\ 2_0_1/GEOSTATReferenceGrid/Grid_ETRS89_LAEA_1K-ref_GEOSTAT_POP_2011_V2_0_1.shp --filtered-grid-filename data/grid_spain.shp
    
    python overlaps_population.py  --layer1 abs_09 --layer2 mitma_mov --filtered-grid-filename data/grid_spain.shp --population-filename data/GEOSTAT-grid-POP-1K-2011-V2-0-1/Version\ 2_0_1/GEOSTAT_grid_POP_1K_2011_V2_0_1.csv
    ```
