import pytz
from area import area
from flowmaps.flowmaps import FlowMaps
from pymongo import InsertOne
from config import mongodb_params, layers
from datetime import datetime
import sys

fm = FlowMaps(mongodb_params)
tz = pytz.timezone('Europe/Madrid')


for layer in layers:
    print('\nLayer:', layer)

    df = fm.layer(layer).get().to_df()

    # compute areas
    df['area'] = df['feat'].apply(lambda x: round(area(x['geometry']) / 1000000, 2))

    # prepare docs
    columns = ['layer', 'id', 'centroid', 'area']
    df = df[columns]
    df['type'] = 'area'
    docs = df.to_dict('records')

    # delete previous documents
    print('Deleting previous documents')
    fm.db['layers.data.consolidated'].delete_many({'type': 'area', 'layer': layer})

    # delete previous provenance
    print('Deleting previous provenance')
    filters = {
        'storedIn': 'layers.data.consolidated',
        'keywords': {
            'type': 'area',
            'layer': layer,
        }
    }
    fm.db['provenance'].delete_many(filters)

    # write provenance
    filters = {'storedIn': 'layers', 'keywords.layer': layer}
    source_provenance = fm.db['provenance'].find_one(filters)
    print(f'Writing provenance')
    doc = {
        'storedAt': tz.localize(datetime.now()),
        'lastStoredAt': None,
        'storedIn': 'layers.data.consolidated',
        'storedBy': sys.argv[0],
        'processedFrom': [source_provenance],
        'keywords': {
            'type': 'area',
            'layer': layer,
        },
        'status': 'started',
        'error': None,
    }
    result = fm.db['provenance'].insert_one(doc)
    provenance_id = result.inserted_id

    # write docs
    try:
        print(f'Writing {len(docs)} documents to: layers.data.consolidated')
        operations = [InsertOne(doc) for doc in docs]
        fm.db['layers.data.consolidated'].bulk_write(operations, ordered=False)
    except Exception as e:
        # update provenance with error
        print(f'ERROR: {e}, updating provenance with error')
        update = {
            '$set': {
                'status': 'failed',
                'error': type(e).__name__,
                'errorAt': tz.localize(datetime.now()),
            }
        }
        fm.db['provenance'].update_one({'_id': provenance_id}, update)
        continue

    # update provenance
    print(f'Updating provenance')
    update = {
        '$set': {
            'status': 'finished',
            'numEntries': len(operations),
            'lastStoredAt': tz.localize(datetime.now()),
        }
    }
    fm.db['provenance'].update_one({'_id': provenance_id}, update)
