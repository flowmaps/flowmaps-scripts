backports-datetime-fromisoformat==1.0.0
decorator==4.4.2
flowmaps @ git+https://gitlab.bsc.es/flowmaps/pyflowmaps.git@e3cd7657ef14d85158103d51ffe801c48d56c5d4
geojson==2.5.0
networkx==2.4
numpy==1.20.1
pandas==1.0.3
plotly==4.14.3
pymongo==3.10.1
python-dateutil==2.8.1
pytz==2019.3
retrying==1.3.3
scipy==1.6.1
six==1.15.0
visvalingamwyatt==0.1.3
