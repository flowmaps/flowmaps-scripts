##
# This script performs the following steps:
#
# Step 1: Download the raw mobility trips made per person stored in mitma_mov.zone_movements day by day
#
# Step 2: Aggregate all the trips for each category (0, 1, 2, 3+ trips) for each day to obtain daily movement matrices.
# The resulting daily matrices contain the total population that have done 0, 1, 2, 3+ trips per day from a source
# mitma_mov area during that day.
#
# Step 3: Project the daily movement matrix to province layer using the overlaps
#
# Step 4: Store the projected matrices
##

import sys
import argparse
import time
import pytz
import json
import numpy as np
from datetime import date, datetime, timezone, timedelta
from scipy.sparse import csr_matrix
from pymongo import InsertOne, DeleteMany, ReplaceOne, UpdateOne
from flowmaps.flowmaps import FlowMaps


def delete_provenance(db, date):
    # delete previous provenance
    print('  PROVENANCE: Deleting previous provenance for date', date)
    filters = {
        'storedIn': 'layers.data.consolidated',
        'keywords': {
            'date': date,
            'type': 'zone_movements',
        }
    }
    db['provenance'].delete_many(filters)


def save_temporal_provenance(db, date, start_date, end_date, status='started'):
    # save temporal provenance
    filters = {
        'storedIn': 'mitma_mov.zone_movements',
        'keywords.evstartMin': {'$gte': start_date, '$lt': end_date},
    }
    source_provenance = list(db['provenance'].find(filters))
    doc = {
        'storedAt': tz.localize(datetime.now()),
        'lastStoredAt': None,
        'storedIn': 'layers.data.consolidated',
        'storedBy': sys.argv[0],
        'processedFrom': source_provenance,
        'keywords': {
            'date': date,
            'type': 'zone_movements',
        },
        'status': status,
        'error': None,
    }
    print(f'  PROVENANCE: Writing temporal provenance for date', date)
    result = db['provenance'].insert_one(doc)
    return result.inserted_id


def finish_update_provenance(db, provenance_id, num_entries):
    # ------------ PROVENANCE (finish-update provenance) --------------
    print(f'  PROVENANCE: Finish provenance')
    update = {
        '$set': {
            'status': 'finished',
            'numEntries': num_entries,
            'lastStoredAt': tz.localize(datetime.now()),
        }
    }
    db['provenance'].update_one({'_id': provenance_id}, update)


def update_provenance(db, provenance_id, update):
    # update provenance with error
    print(f'  PROVENANCE: updating provenance', update)
    update_op = {
        '$set': update
    }
    db['provenance'].update_one({'_id': provenance_id}, update_op)


def write_error_provenance(db, provenance_id, e, error_msg):
    # update provenance with error
    print(f'  PROVENANCE: ERROR: {e}, updating provenance with error')
    update = {
        '$set': {
            'status': 'failed',
            'error': error_msg, #type(e).__name__,
            'errorAt': tz.localize(datetime.now()),
        }
    }
    db['provenance'].update_one({'_id': provenance_id}, update)


# ------------ read commandline args -------------

ap = argparse.ArgumentParser(description="Populate MongoDB collection 'FlowBoard.layers.consolidated.data' with mitma_movements.")

ap.add_argument('-C', '--config', dest="config_filename", required=True, help='Config file with the MongoDB credentials')
ap.add_argument('--skip', default=False, action='store_true', help='Skip already done dates')
ap.add_argument('--first-date', dest="first_date", default=None, type=str, help='First date in format YYYY-mm-dd')
ap.add_argument('--last-date', dest="last_date", default=None, type=str, help='Last date in format YYYY-mm-dd')

args = ap.parse_args()

config_path = args.config_filename

# ------------ read config file ----------------

with open(config_path) as f:
    cfg = json.load(f)

DEST_COLLECTION = cfg['dest_collection']
SOURCE_COLLECTION = cfg['source_collection']
SKIP_ALREADY_DONE_DATES = args.skip
LAYERS_TO_PROJECT = cfg['project_to_layers']
OVERLAPS_COLLECTION = cfg['overlaps_collection']


# ------------ init mongodb ----------------

tz = pytz.timezone('Europe/Madrid')
fm = FlowMaps(cfg['mongo_params'])
db = fm.db

print(f"STARTING at: {datetime.now()}")


# ----------- calculate date ranges ------------

if args.first_date is not None:
    first_date = tz.localize(datetime.strptime(args.first_date, '%Y-%m-%d'))
elif 'first_date' in cfg:
    first_date = tz.localize(datetime.strptime(cfg['first_date'], '%Y-%m-%d'))
else:
    first_date = tz.localize(datetime(2020, 1, 1, 0, 0, 0))

if args.last_date is not None:
    last_date = tz.localize(datetime.strptime(args.last_date, '%Y-%m-%d'))
elif 'last_date' in cfg:
    last_date = tz.localize(datetime.strptime(cfg['last_date'], '%Y-%m-%d'))
else:
    last_date = tz.localize(datetime.combine(date.today(), datetime.min.time()))

if last_date < first_date:
    print(f'Error: first_date ({first_date}) cannot be later than last_date ({last_date})')
    exit(1)


num_days = (last_date - first_date).days
date_range = [(first_date+timedelta(days=x), first_date+timedelta(days=x+1))
              for x in range(num_days+1)]

# Fix tricky problem with hour changes in spain: adding a timedelta(days=1) does
# not correctly handle the one-hour time shift that happens twice a year in spain
# The localization has to be done for each date individually
date_range = [(tz.localize(datetime.strptime(start.strftime('%Y-%m-%d'), '%Y-%m-%d')),
               tz.localize(datetime.strptime(end.strftime('%Y-%m-%d'), '%Y-%m-%d')))
              for start, end in date_range]

print(f"first_date={first_date.strftime('%Y-%m-%d')} - end_date={last_date.strftime('%Y-%m-%d')}")


# ----------- get already aggregated dates ------------

already_done_dates = db[DEST_COLLECTION].find({"type": "zone_movements"}).distinct('date')

print(f'already done dates: {already_done_dates}')


# ----------- get overlaps matrices ---------------

def get_overlaps_matrices(layers, mitma_ids):
    overlaps = {}
    for layer in layers:
        print(f"Getting overlaps matrix for source_layer=mitma_mov, target_layer={layer}")
        layer_ids = fm.layer(layer).get().ids()
        O = fm.overlaps('mitma_mov', layer, collection=OVERLAPS_COLLECTION).get().to_matrix(row_ids=mitma_ids, col_ids=layer_ids)
        overlaps[layer] = {'layer_ids': layer_ids, 'matrix': O}
    return overlaps


mitma_ids = fm.layer('mitma_mov').get().ids()
overlaps_matrices = get_overlaps_matrices(LAYERS_TO_PROJECT, mitma_ids)


# ------------ Start loop (day by day) ----------------

print("\nStarting loop...\n")

for start_date, end_date in date_range:
    date = start_date.strftime('%Y-%m-%d')
    print(f'Starting date: {date}. start_date={start_date}, end_date={end_date}')
    start_time_date = time.time()

    # ------------ check if date already done ----------------
    if SKIP_ALREADY_DONE_DATES and date in already_done_dates:
        print('--- this date has already been aggregated, skipping ---\n')
        continue


    # PROVENANCE: start clean
    delete_provenance(db, date)
    provenance_id = save_temporal_provenance(db, date, start_date, end_date, status='downloading')

    # ------------ Step 1: download raw movements ----------------

    start_time = time.time()
    print(f' Downloading all trips for date={date} (this usually takes about ? min)')

    try:
        cursor = fm.query('mitma_mov.zone_movements')\
                   .set_date_range(start_date, end_date)\
                   .add_date_field()\
                   .project({'id': 1, 'viajes': 1, 'personas': 1})\
                   .get()

        # ------------ Step 2: aggregate daily movements ----------------

        # Convert the documents to a matrix [mitma_areas: 0, 1, 2, 3+ trips], this also performs the aggregation by date
        # [[ 10, 15, 25, 20 ],
        #  [ 70, 30, 23, 15 ],
        #  [ 24, 23, 14, 30 ],
        #        [...]
        # ]
        data = []
        col = []
        row = []
        M_movements = np.zeros((4, len(mitma_ids)))
        for doc in cursor:
            i = int(doc['viajes']) if doc['viajes'] != float('inf') else 3
            j = mitma_ids.index(doc['id'])
            M_movements[i][j] = doc['personas']

        print(" --- finished aggregation. took %s seconds ---\n" % (time.time() - start_time))
    except Exception as e:
        # update provenance with error
        print("ERROR:", type(e).__name__, e)
        msg = f"Exception: {type(e).__name__}. Occurred while downloading data for date={date}, start_date={start_date}, end_date={end_date}"
        write_error_provenance(db, provenance_id, e, msg)
        break


    # PROVENANCE: update provenance status
    update_provenance(db, provenance_id, {'status': 'writing'})

    # ------------ Step 3: Project the daily movement matrix to province layer using the overlaps ----------------

    num_entries = 0

    for projected_layer in LAYERS_TO_PROJECT:
        start_time = time.time()

        print(f'  Converting mobility from mitma_mov-zone_movements to layer={projected_layer}')

        # retrieve saved overlaps matrix for the desired layers
        layer_ids = overlaps_matrices[projected_layer]['layer_ids']
        O_target = overlaps_matrices[projected_layer]['matrix']

        # project trips made person on MITMA areas to target_layer
        M_proj = M_movements * O_target

        # generate documents
        docs = []
        for i, row in enumerate(M_proj):
            for j, persons in enumerate(row):
                doc = {
                    'id': layer_ids[j],
                    'viajes': float(i) if i != 3 else -1,
                    'personas': persons,
                    'layer': projected_layer,
                    'date': date,
                    'updated_at': datetime.now(),
                    'type': 'zone_movements',
                }
                docs.append(doc)

        # delete existing documents to avoid duplications
        filters = {'date': date, 'layer': projected_layer, 'type': 'zone_movements'}
        print(f'  deleting all existing documents matching: {filters}')
        try:
            db[DEST_COLLECTION].delete_many(filters)
        except Exception as e:
            # update provenance with error
            print("ERROR:", type(e).__name__, e)
            msg = f"Exception: {type(e).__name__}. Occurred while deleting previous documents for date={date}, projected_layer={projected_layer}"
            write_error_provenance(db, provenance_id, e, msg)
            break


        if M_proj.sum() == 0:
            print("  WARN: no data for this date to write, skipping..")
            print("  --- took %s seconds\n" % (time.time() - start_time))
            continue


        # write documents to mongo
        operations = [InsertOne(doc) for doc in docs]
        print(f'  writing {len(docs)} documents to: {DEST_COLLECTION}')
        try:
            db[DEST_COLLECTION].bulk_write(operations, ordered=False)
        except Exception as e:
            # update provenance with error
            print("ERROR:", type(e).__name__, e)
            msg = f"Exception: {type(e).__name__}. Occurred while writing documents for date={date}, projected_layer={projected_layer}"
            write_error_provenance(db, provenance_id, e, msg)
            break

        num_entries += len(docs)

        print("  --- took %s seconds\n" % (time.time() - start_time))

    else: # if no break

        # finish-update provenance
        finish_update_provenance(db, provenance_id, num_entries)

    print(" Finished date: %s --- took %s seconds\n" % (date, time.time() - start_time_date))
   
print(f"FINISHED at: {datetime.now()}")
