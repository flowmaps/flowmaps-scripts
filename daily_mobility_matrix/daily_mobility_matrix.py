##
# This script performs the following steps:
#
# Step 1: Download the raw mobility stored in mitma_mov.movements_raw day by day
#
# Step 2: Aggregate all the trips for each day to obtain daily mobility matrices.
# The resulting daily matrices contain the total number of trips from a source mitma_mov
# area to a target mitma_mov area during that day.
# Note: It aggregates all the movements independently of the role_origen and role_destino
#
# Step 3: Store the daily aggregated mitma_mov matrix
#
# Step 4: Project the daily mobility matrix to other layers using the overlaps
#
# Step 5: Store the projected matrices
##


##
# PROVENANCE SCHEME
#
# for each date:
#     1. delete old provenance and save temporal provenance with ('downloading')
#     2. download and aggregate mobility matrix
#     3. if previous step fails, update provenance with ('failed'), else update with ('writing')
#     4. for each pair of layers: project mobility matrix, delete old data and write new data
#     5. if previous step fails deleting or writing, update provenance with ('failed')
#     6. update provenance with ('finished')
##

import sys, os, argparse
import configparser
import time
import pymongo
import pytz
import json
import hashlib
import tempfile
import bson.objectid
from bson.objectid import ObjectId
from scipy.sparse import csr_matrix, eye
from collections import defaultdict
from datetime import date, datetime, timezone, timedelta
from itertools import islice, zip_longest
from pymongo import InsertOne, DeleteMany, ReplaceOne, UpdateOne
from pymongo.errors import BulkWriteError
from flowmaps.flowmaps import FlowMaps


def delete_provenance(db, date):
    # delete previous provenance
    print('  PROVENANCE: Deleting previous provenance for date', date, flush=True)
    filters = {
        'storedIn': 'mitma_mov.daily_mobility_matrix',
        'keywords': {
            'date': date,
        }
    }
    db['provenance'].delete_many(filters)


def save_temporal_provenance(db, date, start_date, end_date, status='started'):
    # save temporal provenance
    filters = {
        'storedIn': 'mitma_mov.movements_raw',
        'keywords.evstartMin': {'$gte': start_date, '$lt': end_date},
    }
    source_provenance = list(db['provenance'].find(filters))
    doc = {
        'storedAt': tz.localize(datetime.now()),
        'lastStoredAt': None,
        'storedIn': 'mitma_mov.daily_mobility_matrix',
        'storedBy': sys.argv[0],
        'processedFrom': source_provenance,
        'keywords': {
            'date': date,
            'layer_pairs': LAYER_PAIRS,
        },
        'status': status,
        'error': None,
    }
    print(f'  PROVENANCE: Writing temporal provenance for date', date, flush=True)
    result = db['provenance'].insert_one(doc)
    return result.inserted_id


def finish_update_provenance(db, provenance_id, num_entries):
    # ------------ PROVENANCE (finish-update provenance) --------------
    print(f'  PROVENANCE: Finish provenance', flush=True)
    update = {
        '$set': {
            'status': 'finished',
            'numEntries': num_entries,
            'lastStoredAt': tz.localize(datetime.now()),
        }
    }
    db['provenance'].update_one({'_id': provenance_id}, update)


def update_provenance(db, provenance_id, update):
    # update provenance with error
    print(f'  PROVENANCE: updating provenance', update, flush=True)
    update_op = {
        '$set': update
    }
    db['provenance'].update_one({'_id': provenance_id}, update_op)


def write_error_provenance(db, provenance_id, e, error_msg):
    # update provenance with error
    print(f'  PROVENANCE: ERROR: {e}, updating provenance with error', flush=True)
    update = {
        '$set': {
            'status': 'failed',
            'error': error_msg, #type(e).__name__,
            'errorAt': tz.localize(datetime.now()),
        }
    }
    db['provenance'].update_one({'_id': provenance_id}, update)


def grouper(iterable, n, fillvalue=None):
    "Collect data into fixed-length chunks or blocks"
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx"
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)


def convert_matrix_to_docs(M, date, source_layer, target_layer, source_ids, target_ids):

    def generate_hash_objectid(d):
        # this ensures a unique index on this fields
        keys = ['source_layer', 'target_layer', 'source', 'target', 'date']
        idstr = '.'.join([d[k] for k in keys])
        hash_24 = hashlib.sha1(idstr.encode('utf-8')).hexdigest()[:24]
        return ObjectId(hash_24)

    M_coo = M.tocoo()
    for i, j, v in zip(M_coo.row, M_coo.col, M_coo.data):
        doc = {
            'source': source_ids[i],
            'target': target_ids[j],
            'trips': v,
            'source_layer': source_layer,
            'target_layer': target_layer,
            'date': date,
            'updated_at': datetime.now()
        }
        doc['_id'] = generate_hash_objectid(doc)
        yield doc


def delete_previous_docs(db, date, source_layer, target_layer):
    # delete existing documents to avoid duplications
    filters = {'date': date, 'source_layer': source_layer, 'target_layer': target_layer}
    print(f'  deleting all existing documents matching: {filters}. Deleting batches of {BATCH_SIZE} documents', end='', flush=True)
    start_time = time.time()
    deleteManyDocuments(db[DEST_COLLECTION], query=filters)
    print(" took %s seconds ---\n" % (time.time() - start_time), flush=True)


def write_docs(db, docs, date, source_layer, target_layer):
    if not docs:
        print("  WARN: no documents to write, skipping..", flush=True)
        return 0

    # write documents to mongo
    print(f'  writing documents to: {DEST_COLLECTION}. Writing batches of {BATCH_SIZE} documents', end='', flush=True)
    start_time = time.time()

    for batch in grouper(docs, BATCH_SIZE):
        print('.', end='', flush=True)
        operations = [InsertOne(doc) for doc in batch if doc]
        db[DEST_COLLECTION].bulk_write(operations, ordered=False)

    print(" took %s seconds ---\n" % (time.time() - start_time), flush=True)


def project_matrix(M, source_layer, target_layer):
    print(f'  Projecting mobility from mitma_mov-mitma_mov to source_layer={source_layer}, target_layer={target_layer}', flush=True)

    # retrieve saved overlaps matrix for the desired layers
    O_source = overlaps_matrices[source_layer]['matrix']
    O_target = overlaps_matrices[target_layer]['matrix']

    # project origin and destination to the desired layers
    M_proj = O_source.T * M * O_target
    return M_proj


def get_overlaps_matrices(layers, mitma_ids):
    overlaps = {}
    for layer in layers:
        print(f"Getting overlaps matrix for source_layer=mitma_mov, target_layer={layer}", flush=True)
        layer_ids = fm.layer(layer).get().ids()
        if layer == 'mitma_mov':
            O = eye(len(layer_ids))
        else:
            O = fm.overlaps('mitma_mov', layer, collection=OVERLAPS_COLLECTION).get().to_matrix(row_ids=mitma_ids, col_ids=layer_ids)
        overlaps[layer] = {'layer_ids': layer_ids, 'matrix': O}
    return overlaps


def download_mobility_matrix(date, start_date, end_date, mitma_ids):
    start_time = time.time()

    print(f'  Downloading all movements for date={date} (this usually takes about 5-10 min)', flush=True)

    query = fm.mobility('mitma_mov.movements_raw')

    query = query.set_date_range(start_date, end_date)\
                 .add_date_field()\
                 .project({'destino': 1, 'origen': 1, 'viajes': 1, 'viajes_km': 1 })

    cursor = query.get()

    # convert to matrix, this also performs the aggregation
    M = cursor.to_matrix('viajes', mitma_ids)

    print("  --- finished downloading. took %s seconds ---\n" % (time.time() - start_time), flush=True)
    return M


def download_daily_mobility_matrix(date):
    start_time = time.time()
    print(f'  Downloading all movements for date={date} (this usually takes about 5-10 min)', flush=True)

    collection = "mitma_mov.daily_mobility_matrix"
    query = fm.mobility(collection)

    filters = {"date":date, "source_layer":"mitma_mov", "target_layer":"mitma_mov"}
    query = query.filter(filters)
    cursor = query.get()
    
    mitma_ids = fm.layer('mitma_mov').get().ids()
    M = cursor.to_matrix('trips', mitma_ids, source_field='source', target_field='target')

    print("  --- finished downloading. took %s seconds ---\n" % (time.time() - start_time), flush=True)
    return M



def check_date(strn_date, source_layer, target_layer):
    filters = { 
        "$match": {
            "date":strn_date, 
            "source_layer":source_layer, 
            "target_layer":target_layer
        }
    }
    projection = { "$project": {"date": 1} }
    limit = { "$limit": 1 }
    pipeline = [filters, projection, limit]
    cursor = db["mitma_mov.daily_mobility_matrix"].aggregate(pipeline)

    return len([i for i in cursor]) > 0

def deleteManyDocuments(collObj, query=None):
    """ Copied and adapted from here: https://gitlab.bsc.es/flowmaps/mov_loader/-/blob/master/mov_loader/abstract.py#L291"""

    if not isinstance(query,dict):
        query={}
    
    numRemoved = 0
    with tempfile.TemporaryFile(buffering=1024*1024,prefix='mongo',suffix='batch') as ids_find:
        numToErase = 0
        try:
            for ent in collObj.find(query,['_id']):
                numToErase += 1
                ids_find.write(ent['_id'].binary)
        except (AttributeError, OSError):
            # This is needed for cases where the _id is not a OID
            # or there is no left space for temporary files
            return collObj.bulk_write([pymongo.operations.DeleteMany(query)],ordered=False).deleted_count
            
        #print("DEBUG toErase {}".format(numToErase),file=sys.stderr)
        # Now, let's erase the batched ids!
        if numToErase > 0:
            ids_find.seek(0)
            batNum = 0
            batchOps = []
            delarr = []
            # 12 is the size of a binary OID
            oid = ids_find.read(12)
            while len(oid) == 12:
                batchOps.append(bson.objectid.ObjectId(oid))
                batNum += 1
                if batNum >= BATCH_SIZE:
                    delarr.append(pymongo.DeleteMany({'_id': {'$in': batchOps}}))
                    batchOps = []
                    batNum = 0
                    if len(delarr) >= 10:
                        print('.', end='', flush=True)
                        res = collObj.bulk_write(delarr,ordered=False)
                        numRemoved += res.deleted_count
                        delarr = []
                        #print("DEBUG {} {} {} {}".format(datetime.datetime.now().isoformat(),numRemoved,numToErase,float(numRemoved)/numToErase),file=sys.stderr)
                        sys.stderr.flush()
                oid = ids_find.read(12)
            if batNum > 0:
                delarr.append(pymongo.DeleteMany({'_id': {'$in': batchOps}}))
                print('.', end='', flush=True)
                res = collObj.bulk_write(delarr,ordered=False,bypass_document_validation=False)
                numRemoved += res.deleted_count
                #print("DEBUG {} {} {} {}".format(datetime.datetime.now().isoformat(),numRemoved,numToErase,float(numRemoved)/numToErase),file=sys.stderr)
    
    return numRemoved



# ------------ read comandline args -------------

ap = argparse.ArgumentParser(description="")

ap.add_argument('-C','--config',dest="config_filename",required=True,help='Config file with the MongoDB credentials')
ap.add_argument('--skip',default=False,action='store_true',help='Skip already done dates')
ap.add_argument('--first-date',dest="first_date",default=None,type=str,help='First date in format YYYY-mm-dd')
ap.add_argument('--last-date',dest="last_date",default=None,type=str,help='Last date in format YYYY-mm-dd')
ap.add_argument('--fail',default=False,action='store_true',help='Fail when some exception occurs')

args = ap.parse_args()

config_path = args.config_filename

# ------------ read config file ----------------

with open(config_path) as f:
    cfg = json.load(f)


DEST_COLLECTION = cfg['dest_collection']
SOURCE_COLLECTION = cfg['source_collection']
SKIP_ALREADY_DONE_DATES = args.skip
LAYER_PAIRS = cfg['project_to_layer_pairs']
ALL_LAYERS = [pair[0] for pair in LAYER_PAIRS]
ALL_LAYERS += [pair[1] for pair in LAYER_PAIRS]
ALL_LAYERS = list(set(ALL_LAYERS))
OVERLAPS_COLLECTION = cfg['overlaps_collection']
BATCH_SIZE = 1000
FAIL_WHEN_EXCEPTION = args.fail


# ------------ init mongodb ----------------

tz = pytz.timezone('Europe/Madrid')
fm = FlowMaps(cfg['mongo_params'])
db = fm.db

print(f"STARTING SCRIPT at: {datetime.now()}", flush=True)


# ----------- calculate date ranges ------------

if args.first_date is not None:
    first_date = tz.localize(datetime.strptime(args.first_date, '%Y-%m-%d'))
elif 'first_date' in cfg:
    first_date = tz.localize(datetime.strptime(cfg['first_date'], '%Y-%m-%d'))
else:
    first_date = tz.localize(datetime(2020, 1, 1, 0, 0, 0))

if args.last_date is not None:
    last_date = tz.localize(datetime.strptime(args.last_date, '%Y-%m-%d'))
elif 'last_date' in cfg:
    last_date = tz.localize(datetime.strptime(cfg['last_date'], '%Y-%m-%d'))
else:
    # last_date = datetime(2020, 6, 10, 0, 0, 0, tzinfo=tz)
    last_date = tz.localize(datetime.combine(date.today(), datetime.min.time()))

if last_date < first_date:
    print(f'Error: first_date ({first_date}) cannot be later than last_date ({last_date})', flush=True)
    exit(1)


num_days = (last_date - first_date).days
date_range = [(first_date+timedelta(days=x), first_date+timedelta(days=x+1))
              for x in range(num_days+1)]

# Fix tricky problem with hour changes in spain: adding a timedelta(days=1) does
# not correctly handle the one-hour time shift that happens twice a year in spain
# The localization has to be done for each date individually
date_range = [(tz.localize(datetime.strptime(start.strftime('%Y-%m-%d'), '%Y-%m-%d')),
               tz.localize(datetime.strptime(end.strftime('%Y-%m-%d'), '%Y-%m-%d')))
              for start, end in date_range]

print(f"first_date={first_date.strftime('%Y-%m-%d')} - last_date={last_date.strftime('%Y-%m-%d')}", flush=True)


# ----------- get already aggregated dates ------------

already_done_dates = db[DEST_COLLECTION].distinct('date')

print(f'already done dates: min={min(already_done_dates)}, max={max(already_done_dates)}', flush=True)


# ----------- get overlaps matrices ---------------

mitma_ids = fm.layer('mitma_mov').get().ids()
overlaps_matrices = get_overlaps_matrices(ALL_LAYERS, mitma_ids)


# ------------ Start loop (day by day) ----------------

print("\nStarting loop...\n", flush=True)

for start_date, end_date in date_range:
    # print(start_date, '-', end_date)
    strn_date = start_date.strftime('%Y-%m-%d')
    print(f'Starting date: {strn_date}. start_date={start_date}, end_date={end_date}', flush=True)
    start_time_for_iteration = time.time()


    # check if date already done
    # if SKIP_ALREADY_DONE_DATES and strn_date in already_done_dates:
    #     print('--- this date has already been aggregated, skipping ---\n', flush=True)
    #     continue

    # delete previous provenance and save temporal provenance
    delete_provenance(db, strn_date)
    provenance_id = save_temporal_provenance(db, strn_date, start_date, end_date, status='downloading')

    
        # download mobility matrix
        # M = download_mobility_matrix(strn_date, start_date, end_date, mitma_ids)
    try:
        if check_date(strn_date, "mitma_mov","mitma_mov"):
            M = download_daily_mobility_matrix(strn_date)
        else:
            M = download_mobility_matrix(strn_date, start_date, end_date, mitma_ids)
    except Exception as e:
            # update provenance with error
            print("ERROR downloading mobility matrix:", type(e).__name__, e, flush=True)
            msg = f"Exception: {type(e).__name__}. Occurred while downloading data for date={strn_date}, start_date={start_date}, end_date={end_date}"
            write_error_provenance(db, provenance_id, e, msg)
            if FAIL_WHEN_EXCEPTION:
                raise
            else:
                break

    # update provenance status
    update_provenance(db, provenance_id, {'status': 'writing'})

    num_entries = 0

    for source_layer, target_layer in LAYER_PAIRS:
        source_ids = overlaps_matrices[source_layer]['layer_ids']
        target_ids = overlaps_matrices[target_layer]['layer_ids']

        already_done = check_date(strn_date, source_layer,target_layer)
        if already_done and SKIP_ALREADY_DONE_DATES:
            print(f'Skipping date {strn_date} for layers pair {source_layer} {target_layer}')
            continue

        start_time = time.time()

        # project matrix using overlaps
        M_proj = project_matrix(M, source_layer, target_layer)

        # delete previous documents
        try:
            delete_previous_docs(db, strn_date, source_layer, target_layer)
        except Exception as e:
            # update provenance with error
            print("ERROR deleting documents:", type(e).__name__, e, flush=True)
            msg = f"Exception: {type(e).__name__}. Occurred while deleting previous documents for date={strn_date}, source_layer={source_layer}, target_layer={target_layer}"
            write_error_provenance(db, provenance_id, e, msg)
            if FAIL_WHEN_EXCEPTION:
                raise
            else:
                break

        # generate documents
        print(f'  Generating {len(M_proj.data)} documents. Maximum connectivity would be: {M_proj.shape[0]*M_proj.shape[1]} shape={M_proj.shape}.', flush=True)
        docs = convert_matrix_to_docs(M_proj, strn_date, source_layer, target_layer, source_ids, target_ids)

        # store documents
        try:
            write_docs(db, docs, strn_date, source_layer, target_layer)
        except Exception as e:
            # update provenance with error
            print("ERROR storing documents:", type(e).__name__, e, flush=True)
            msg = f"Exception: {type(e).__name__}. Occurred while writing documents for date={strn_date}, source_layer={source_layer}, target_layer={target_layer}"
            write_error_provenance(db, provenance_id, e, msg)
            if FAIL_WHEN_EXCEPTION:
                raise
            else:
                break

        total_docs = len(list(docs))
        assert num_entries <= (num_entries + total_docs), f"{num_entries} <= {(num_entries + total_docs)}"  # check for overflow
        num_entries += total_docs

        print("  --- took %s seconds\n" % (time.time() - start_time), flush=True)

    else: # if no break

        # finish-update provenance
        finish_update_provenance(db, provenance_id, num_entries)

    print("Finished date: %s --- took %s seconds\n" % (strn_date, time.time() - start_time_for_iteration), flush=True)


print(f"FINISHED SCRIPT at: {datetime.now()}", flush=True)
