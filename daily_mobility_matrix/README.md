
# Script to populate mitma_mov.daily_mobility_matrix collection

This script aggregates the movements stored in **mitma_mov.movements_raw** by day, and stores the results in **mitma_mov.daily_mobility_matrix**.

It also transforms and stores the mobility projected to other layers. For example, the daily mobility between provinces (layer cnig_provinces) or between ABS (layer abs_09).

Input collections:

- mitma_mov.movements_raw 	(hourly movements)

Output collections:

- mitma_mov.daily_mobility_matrix 	(daily movements)


## Installation

Look at **install.sh** script


## Configuration

Create a config.ini file. Example:

	[DEFAULT] 
	mongo_host = mongomachine:27017
	mongo_username = flowmaps_daily_mobility_matrix
	mongo_password = ****
	mongo_authsource = FlowMaps
	batch_size = 1000
	dest_collection = mitma_mov.daily_mobility_matrix
	source_collection = mitma_mov.movements_raw
	skip_already_done_dates = False
	first_date = 2020-09-12
	last_date = 2020-09-12
	project_to_layers = cnig_provincias, cnig_ccaa, abs_09, zbs_15, zbs_07, oe_16, zon_bas_13
