#!/bin/bash

set -e

echo "START script. Date: $(date -Is)"

cd "$(dirname "$0")"

source env/bin/activate

mkdir -p logs

python3 daily_mobility_matrix.py --config config.json --skip >> logs/cron.log 2>&1

echo "FINISH script. Date: $(date -Is)"


