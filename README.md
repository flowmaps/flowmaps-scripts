# ETL scripts

This repository contains the ETL scripts used to process the data and update/populate some of the collections.

Collections created and populated using this scripts:

- mitma_mov.daily_mobility_matrix
- layers.data.consolidated
- layers.overlaps_population

Look at the README.md on each subfolder for more details.
